const Login = () => import('~/pages/auth/login').then(m => m.default || m)
const Register = () => import('~/pages/auth/register').then(m => m.default || m)
const NotFound = () => import('~/pages/errors/404').then(m => m.default || m)

const NoteCreate = () => import('~/pages/note/create').then(m => m.default || m)
const NoteEdit = () => import('~/pages/note/edit').then(m => m.default || m)

const About = () => import('~/pages/about').then(m => m.default || m)

const Home = () => import('~/pages/home').then(m => m.default || m)

export default [
//  { path: '/', name: 'welcome', component: Welcome },

  { path: '/login', name: 'login', component: Login },
  { path: '/register', name: 'register', component: Register },

  { path: '/note/create', name: 'note.create', component: NoteCreate },
  { path: '/note/edit/:token', name: 'note.edit', component: NoteEdit },

  { path: '/', name: 'home', component: Home },
  { path: '/about', name: 'about', component: About },

  { path: '*', component: NotFound }
]
