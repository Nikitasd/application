<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class NoteResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at);
        
        return [
            'type'          => 'notes',
            'id'            => (string)$this->id,
            'attributes'    => [
                'title' => $this->title,
                'description'=> $this->description,
                'token'=> $this->token,
                'created_at' => $date->toFormattedDateString()
            ],

            'relationships' => new NoteRelationshipResource($this),

            'links'    => [
                'self' => route('notes.show',['id' => $this->token]),
            ],
        ];
    }
}
