<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\NoteResource;
use Illuminate\Http\Response;
use App\Note;
use Auth;

class NoteController extends Controller
{
    public function index()
    {
        $articles = Note::with([
            'user:users.id,users.email',
        ])->advancedFilter();

        $collection = NoteResource::collection($articles);

        return $collection->response()
            ->setStatusCode(Response::HTTP_PARTIAL_CONTENT);
    }

    public function show(Note $note)
    {
        NoteResource::withoutWrapping();
        return new NoteResource($note);
    }


    public function add(Request $request)
    {
        $data = array_merge($request->all(), [
            'user_id' => Auth::id()
        ]);

        $note = new Note();
        $note->fill($data);

     if($note->save()){
        return response()
            ->json(['status' => "success", 'data' => [
                'title' => $request->title,
                'description' => $request->description,
                'token' => $request->token,
            ]]);
        } else {
            return response()
                ->json(['status' => "error"]);
        }

    }

    public function edit(Request $request, $id)
    {
        $data = array_merge($request->all(), [
            'user_id' => Auth::id()
        ]);

        $note = Note::find($request->id);

        if( $note->update($data)){
            return response()
                ->json(['status' => "success", 'data' => [
                    'title' => $request->title,
                    'description' => $request->description,
                    'token' => $request->token,
                ]]);
        } else {
            return response()
                ->json(['status' => "error"]);
        }

        //return $this->response->withNoContent();
    }

    public function destroy(Request $request, $id)
    {
        Note::where('id', $request->id)->delete();
    }

    private function statusSuccess()
    {

    }


}
