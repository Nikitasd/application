<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Illuminate\Database\Eloquent\SoftDeletes;

class Note extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'title',
        'token',
        'description'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeAdvancedFilter($query)
    {
        $data = $this->validateAdvancedFilter(request()->all());

        return $query->paginate($data['limit']);
    }

    protected function validateAdvancedFilter(array $request)
    {
        $request['limit'] = $request['limit'] ?? 12;

        $validator = validator()->make($request, [
            'limit' => 'sometimes|required|integer|min:1|max:12',
            'page' => 'sometimes|required|integer|min:1',
        ]);


        return $validator->validate();
    }

    public function getRouteKeyName()
    {
        return 'token';
    }
}
