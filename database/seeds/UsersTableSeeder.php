<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'email' => 'user@gmail.com',
                'password' => bcrypt("user")
            ]
        ];

        DB::table('users')->insert($users);

        factory(User::class, 3)->create();
    }
}
