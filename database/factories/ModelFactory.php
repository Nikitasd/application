<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {

    return [
        'email' => $faker->unique()->safeEmail,
        'password' => bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Note::class, function (Faker $faker) {
    $user_id = \App\User::pluck('id')->random();

    return [
        'user_id'      => $user_id,
        'title'    => $faker->sentence(mt_rand(3,10)),
        'description'  => $faker->paragraph,
        'token'  => $faker->paragraph,
    ];
});
