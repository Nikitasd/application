# Api application
Написано на Laravel + Vue.js (SPA).
Клиентская часть находится в /resources/js


### Installation
`git clone gitlab.com/Nikitasd/application`

`cd application`

`composer install`

`npm install`

`php artisan migrate --seed`


#### Несколько фотографий ()
<p align="center"><img src="https://i.imgur.com/FLiD610.png"></p>
<p align="center"><img src="https://i.imgur.com/SeYy23Q.png"></p>

На верстку не делал почти упор.

Краткое описание нескольких api запросов можно посмотреть на странице /about